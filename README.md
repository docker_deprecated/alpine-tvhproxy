# alpine-tvhproxy

#### [alpine-x64-tvhproxy](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-tvhproxy/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64build/alpine-x64-tvhproxy.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-tvhproxy "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64build/alpine-x64-tvhproxy.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-tvhproxy "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-tvhproxy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-tvhproxy/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-tvhproxy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-tvhproxy/)
#### [alpine-aarch64-tvhproxy](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy.svg)](https://microbadger.com/images/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy.svg)](https://microbadger.com/images/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy/)
#### [alpine-armhf-tvhproxy](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy.svg)](https://microbadger.com/images/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy.svg)](https://microbadger.com/images/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : tvhProxy
    - A small flask app to proxy requests between Plex Media Server and Tvheadend.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 5004:5004/tcp \
           -e TVH_URL="http://localhost:9981" \
           forumi0721alpinex64build/alpine-x64-tvhproxy:latest
```

* aarch64
```sh
docker run -d \
           -p 5004:5004/tcp \
           -e TVH_URL="http://localhost:9981" \
           forumi0721alpineaarch64build/alpine-aarch64-tvhproxy:latest
```

* armhf
```sh
docker run -d \
           -p 5004:5004/tcp \
           -e TVH_URL="http://localhost:9981" \
           forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy:latest
```



----------------------------------------
#### Usage

* tvhproxy address : localhost:5004



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 5004/tcp           | Serivce port                                     |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| TVH_URL            | TVHeadend URL                                    |



----------------------------------------
* [forumi0721alpinex64build/alpine-x64-tvhproxy](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-tvhproxy/)
* [forumi0721alpineaarch64build/alpine-aarch64-tvhproxy](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-tvhproxy/)
* [forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-tvhproxy/)

